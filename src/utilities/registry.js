// Controllers
import Home from 'Pages/Home';
import PortfolioPage from 'Pages/PortfolioPage';
import Articles from 'Pages/Articles';
import BlogsPage from 'Pages/BlogsPage';
import BiographyPage from 'Pages/BiographyPage';
import ContactAndSocialMediaPage from 'Pages/ContactAndSocialMediaPage';

function Injector(component, name) {
 const hoc = component;
 hoc.displayName = name;

 return hoc;
}

export default {
 Home: Injector(Home, 'Home'),
 PortfolioPage: Injector(PortfolioPage, 'PortfolioPage'),
 Articles: Injector(Articles, 'Articles'),
 BlogsPage: Injector(BlogsPage, 'BlogsPage'),
 BiographyPage: Injector(BiographyPage, 'BiographyPage'),
 ContactAndSocialMediaPage: Injector(ContactAndSocialMediaPage, 'ContactAnd')
};
