import React from 'react';
import './index.css';

const Loader = () => {
 return (
  <div className="loader-cont">
   <div className="loader-cat-portion">
    <div className="cat">
     <div className="ear ear--left"></div>
     <div className="ear ear--right"></div>
     <div className="face">
      <div className="eye eye--left">
       <div className="eye-pupil"></div>
      </div>
      <div className="eye eye--right">
       <div className="eye-pupil"></div>
      </div>
      <div className="muzzle"></div>
     </div>
    </div>
   </div>
   <div className="welcome-quotes">
    Hello, traveler! Embark on an adventure with{' '}
    <span className="welcome-quotes-web-name">Ms.Comrade</span>
   </div>
   <div className="loader-cont">
    <div className="loader"></div>
   </div>
  </div>
 );
};

export default Loader;
