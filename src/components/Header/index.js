import React, { useState, useEffect } from 'react';
import './index.css';
import webLogo from 'Assets/icons/webLogo.png';

const Header = () => {
 const [selectedPage, setSelectedPage] = useState(2);
 const handleHeaderPage = (index) => {
  console.log(index, 'indexing');
  setSelectedPage(index);
 };

 return (
  <div className="header-cont">
   <div className="header-left-content">
    <a href="/">
     <div className="header-web-logo">
      <img src={webLogo} alt="Ms.Comrade" />
     </div>
    </a>

    <a
     className="header-tags"
     href="/portfolio"
     style={{
      color:
       (window.location.pathname === '/portfolio' ||
        window.location.pathname === '/' ||
        window.location.pathname === '/home') &&
       'rgba(255, 113, 65, 225)',
      fontSize:
       (window.location.pathname === '/portfolio' ||
        window.location.pathname === '/' ||
        window.location.pathname === '/home') &&
       '20px',

      textShadow:
       (window.location.pathname === '/portfolio' ||
        window.location.pathname === '/' ||
        window.location.pathname === '/home') &&
       '-2px 2px 9px rgba(189,70,47,1)'
     }}
     onClick={() => {
      handleHeaderPage(1);
     }}>
     Portfolio
    </a>

    <a
     href="/blogs"
     onClick={() => handleHeaderPage(2)}
     style={{
      color: window.location.pathname === '/blogs' && 'rgba(255, 113, 65, 225)',
      textShadow: window.location.pathname === '/blogs' && '-2px 2px 9px rgba(189,70,47,1)'
     }}>
     Blogs
    </a>
    <a
     href="/articles"
     style={{
      color: window.location.pathname === '/articles' && 'rgba(255, 113, 65, 225)',
      textShadow: window.location.pathname === '/articles' && '-2px 2px 9px rgba(189,70,47,1)'
     }}
     onClick={() => handleHeaderPage(3)}>
     Articles
    </a>
    <a
     href="/biography"
     style={{
      color: window.location.pathname === '/biography' && 'rgba(255, 113, 65, 225)',
      textShadow: window.location.pathname === '/biography' && '-2px 2px 9px rgba(189,70,47,1)'
     }}
     onClick={() => handleHeaderPage(4)}>
     Biography
    </a>
   </div>
   <div className="header-right-content">
    <a
     href="/contactAndSocialMedia"
     style={{
      color: window.location.pathname === '/contactAndSocialMedia' && 'rgba(255, 113, 65, 225)',
      textShadow:
       window.location.pathname === '/contactAndSocialMedia' && '-2px 2px 9px rgba(189,70,47,1)'
     }}
     onClick={() => handleHeaderPage(5)}>
     Contact
    </a>
   </div>
  </div>
 );
};

export default Header;
