import React from 'react';
import './index.css';
import Header from '../../components/Header';
import Footer from '../../components/Footer';

const PortfolioPage = () => {
 return (
  <div className="portfolio-container">
   <Header />
   its Portfolio Page
   <Footer />
  </div>
 );
};

export default PortfolioPage;
