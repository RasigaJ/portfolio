import React from 'react';
import './index.css';
import Header from 'Components/Header';
import Footer from 'Components/Footer';
import reactImg from 'Assets/images/react_banner.jpg';

const BlogsPage = () => {
 return (
  <div className="blogs-container">
   <Header />
   <div className="blogs-content">
    <div className="blogs-welcome-words">
     <span>
      Step into the realm of my tech blog, where insights meet innovation and experiences blend with
      expertise. Here, I extend a warm invitation to explore a treasure trove of knowledge curated
      from my journey in the ever-evolving world of technology.
     </span>
     <span>
      Together, let's navigate the digital landscape and empower ourselves with the tech prowess to
      endeavor.
     </span>
    </div>
    <div className="blogs-portion">
     {/*  */}
     <div className="blogs-division-wrapper">
      <div className="blog-details">
       <div className="blog-title-image">
        <img src={reactImg} alt="React JS" />
       </div>
       <div className="blog-summary">
        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
        been the industry's standard dummy text ever since the 1500s, when an unknown printer took a
        galley of type and scrambled it to make a type specimen book. It has survived not only five
        centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
        It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum
        passages, and more recently with desktop publishing software like Aldus PageMaker including
        versions of Lorem Ipsum.
       </div>
      </div>

      <div className="blog-details" style={{ flexDirection: 'row-reverse' }}>
       <div className="blog-title-image">
        <img src={reactImg} alt="React JS" />
       </div>
       <div className="blog-summary">
        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
        been the industry's standard dummy text ever since the 1500s, when an unknown printer took a
        galley of type and scrambled it to make a type specimen book. It has survived not only five
        centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
        It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum
        passages, and more recently with desktop publishing software like Aldus PageMaker including
        versions of Lorem Ipsum.
       </div>
      </div>
     </div>
     {/*  */}
    </div>
   </div>
   <Footer />
  </div>
 );
};

export default BlogsPage;
