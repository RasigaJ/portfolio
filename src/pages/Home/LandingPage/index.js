import React from 'react';
import './index.css';
import Footer from 'Components/Footer';
import ProfilePage from '../../PortfolioPage';
import WorkExperiencePage from '../../Articles';
import BlogsPage from '../../BlogsPage';
import BiographyPage from '../../BiographyPage';
import ContactAndSocialMediaPage from '../../ContactAndSocialMediaPage';
import Header from '../../../components/Header';

const LandingPage = () => {
 return (
  <div>
   <Header />
   <ProfilePage />
   <WorkExperiencePage />
   <BlogsPage />
   <BiographyPage />
   <ContactAndSocialMediaPage />
   <Footer />
  </div>
 );
};

export default LandingPage;
