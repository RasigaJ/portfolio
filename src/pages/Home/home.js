import React, { useContext, useEffect, useState } from 'react';
import './index.css';
import RefContext from 'Utilities/refContext';
import Loader from '../../components/Loader';
import LandingPage from './LandingPage';
import PortfolioPage from '../PortfolioPage';

const Homepage = () => {
 const ctx = useContext(RefContext);
 const { store, actions } = ctx;
 const { getAllRequetUser } = actions;
 const { testData } = store;
 const [loading, setLoading] = useState(true);

 useEffect(() => {
  const timeout = setTimeout(() => {
   setLoading(false);
  }, 5000);
  return () => clearTimeout(timeout);
 }, []);

 return (
  <div className={loading ? 'home-content-loader-page' : 'home-content'}>
   {loading ? <Loader /> : <PortfolioPage />}
   {/* <Loader /> */}
  </div>
 );
};

export default Homepage;
