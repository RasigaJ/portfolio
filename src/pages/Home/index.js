import React from 'react';

import RefProvider from 'Utilities/refProvider';
import RefErrorBoundary from 'Utilities/refErrorBoundary';
import { formStoreData } from 'Utilities/helpers';
import Homepage from './home';
import {} from 'react-router-dom';
import PortfolioPage from '../PortfolioPage';

const Home = (props) => {
 const propShape = formStoreData(props, ['dashboard']);
 return (
  <>
   <RefProvider data={propShape}>
    <RefErrorBoundary {...props}>
     <PortfolioPage />
    </RefErrorBoundary>
   </RefProvider>
  </>
 );
};

export default Home;
