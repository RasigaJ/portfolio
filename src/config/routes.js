import React from 'react';
import { createBrowserRouter, RouterProvider } from 'react-router-dom';

const fetchRoutes = (containers) => {
 const { Home } = containers;
 const { PortfolioPage } = containers;
 const { Articles } = containers;
 const { BlogsPage } = containers;
 const { BiographyPage } = containers;
 const { ContactAndSocialMediaPage } = containers;

 return function Routes() {
  const createRoutes = () => {
   return createBrowserRouter([
    {
     path: '/',
     element: <Home />,
     errorElement: <div>404 No Page Found</div>
    },
    {
     path: '/home',
     element: <Home />,
     errorElement: <div>404 No Page Found</div>
    },
    {
     path: '/portfolio',
     element: <PortfolioPage />,
     errorElement: <div>404 No Page Found</div>
    },
    {
     path: '/blogs',
     element: <BlogsPage />,
     errorElement: <div>404 No Page Found</div>
    },
    {
     path: '/articles',
     element: <Articles />,
     errorElement: <div>404 No Page Found</div>
    },
    {
     path: '/biography',
     element: <BiographyPage />,
     errorElement: <div>404 No Page Found</div>
    },
    {
     path: '/contactAndSocialMedia',
     element: <ContactAndSocialMediaPage />,
     errorElement: <div>404 No Page Found</div>
    }
   ]);
  };
  return (
   <>
    <RouterProvider router={createRoutes()} />
   </>
  );
 };
};

export { fetchRoutes };
